"use strict";
exports.__esModule = true;
var helpers_1 = require("@turf/helpers");
var circle_1 = require("@turf/great-circle");
var center = helpers_1.point([-75.343, 39.984]);
var units = 'kilometers';
var radius = 5;
var steps = 10;
circle_1["default"](center, radius);
circle_1["default"](center, radius, { steps: steps });
circle_1["default"](center, radius, { steps: steps, units: units });
circle_1["default"]([-75, 39], radius, { steps: steps, units: units, properties: { foo: 'bar' } });

